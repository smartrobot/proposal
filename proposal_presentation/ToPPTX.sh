#!/bin/bash
# My first script

mkdir -p images && pdftoppm -jpeg -r 300 pres.pdf images/pg

echo 1/4: Pdf convert to JPGs done.

img2pdf images/*.jpg -o presentation_pre_ppt.pdf

echo 2/4: JPGs convert to flat pdf done.

soffice --convert-to odg presentation_pre_ppt.pdf

echo 3/4: pdf convert to odg done.

soffice --convert-to pptx presentation_pre_ppt.odg

echo 4/4: pdf convert to pptx done.
