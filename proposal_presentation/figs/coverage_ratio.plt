set term fig color fontsize 10 thickness 2
set output "coverage_ratio.fig"
set title " "

set xlabel "$\\theta_{max}$ In Degrees"
set ylabel "Environment Ratio Covered"

set size 1, 1

set xtics 0.5
set mxtics 5
set xrange [0:3.5]
set ytics 0.1
set mytics 5
set yrange [0:1]

plot  "cave.dat" using 1:2 w linespoints pt 7 title "cave", \
      "mini_maze.dat" using 1:2 w linespoints pt 9 title "maze", \
      "brick.dat" using 1:2 w linespoints pt 5 title "brick", \
      "test-interval_calculator.dat" using 1:2 w linespoints pt 9 title "open"
